from flask import Flask, render_template
import requests
import random

app = Flask(__name__)

@app.route('/')
def hello():
    list_din = requests.get('https://medusa.delahayeyourself.info/api/dinosaurs/').json()
    return render_template('index.html', list_din= list_din)

@app.route('/dinosaur/<slugDuDinosaure>')
def dinosaur(slugDuDinosaure):
    list_din = requests.get('https://medusa.delahayeyourself.info/api/dinosaurs/').json()
    res = False

    for dino in list_din:
        if dino['slug'] == slugDuDinosaure:
            res = True
            din_uri = dino.get("uri")
            dino_res = requests.get(str(din_uri)).json()
            dino_alea = random.sample(list_din,3)

    if res:
        return render_template('slugdino.html', dino_res= dino_res, dino_alea = dino_alea)
    else:
        return "404"




    #slug din = requests.get("uriiiiiiiii").json()
    #return render_template('slugdino.html', slug_din= slug_din)

    
