import os
import pytest
from app import app
from flask import url_for
import requests

@pytest.fixture
def client():
    app.config['TESTING'] = True
    app.config['SERVER_NAME'] = 'test'
    client = app.test_client()
    with app.app_context():
        pass
    app.app_context().push()
    yield client

def test_index(client):
    rv = client.get('/')
    assert rv.status_code == 200
    assert b'The world of Jurassic Park' in rv.data

def test_api(client):
    rv = requests.get('https://medusa.delahayeyourself.info/api/dinosaurs/')
    assert rv.status_code == 200

def test_liens_dino(client):
    dinoo = "triceratops"
    rv = requests.get('https://medusa.delahayeyourself.info/api/dinosaurs/' + dinoo)
    assert rv.status_code == 200
    dinoo = "gallimimus"
    rv = requests.get('https://medusa.delahayeyourself.info/api/dinosaurs/' + dinoo)
    assert rv.status_code == 200